Simple program that outputs PEG ast tree
for given grammar and input text.

It is intended to be used with term rewriting systems or as a grep.

# Requirements
- [GNU guile](https://www.gnu.org/software/guile/)
 that provides (ice-9 peg)

